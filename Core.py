from experta import *


class Core(KnowledgeEngine):

    # Best time
    ####
    def init_best_time(self, name, skipped=False):
        self.declare(Fact(best_time_provided=True))
        self.declare(Fact(best_time_may_be_line=(name == 'line') or skipped))
        self.declare(Fact(best_time_may_be_square=(name == 'square') or skipped))
        self.declare(Fact(best_time_may_be_nlogn=(name == 'nlogn') or skipped))
        self.declare(Fact(best_time_may_be_nlog2n=(name == 'nlog2n') or skipped))
        self.declare(Fact(best_time_may_be_nloglogn=(name == 'nloglogn') or skipped))
        self.declare(Fact(best_time_may_be_nlgn=(name == 'nlgn') or skipped))
        self.declare(Fact(best_time_may_be_linePlusK=(name == 'linePlusK') or skipped))
        self.declare(Fact(best_time_may_be_mutyTredLogn=(name == 'mutyTredLogn') or skipped))

    # Best time lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(best_time='line'),
          NOT(Fact(best_time_provided=W())))
    def line_best_time_true(self):
        self.init_best_time('line')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='square'),
          NOT(Fact(best_time_provided=W())))
    def square_best_time_true(self):
        self.init_best_time('square')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='nlogn'),
          NOT(Fact(best_time_provided=W())))
    def nlogn_best_time_true(self):
        self.init_best_time('nlogn')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='nlog2n'),
          NOT(Fact(best_time_provided=W())))
    def nlog2n_best_time_true(self):
        self.init_best_time('nlog2n')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='nloglogn'),
          NOT(Fact(best_time_provided=W())))
    def nloglogn_best_time_true(self):
        self.init_best_time('nloglogn')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='nlgn'),
          NOT(Fact(best_time_provided=W())))
    def nlgn_best_time_true(self):
        self.init_best_time('nlgn')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='linePlusK'),
          NOT(Fact(best_time_provided=W())))
    def linePlusK_best_time_true(self):
        self.init_best_time('linePlusK')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='mutyTredLogn'),
          NOT(Fact(best_time_provided=W())))
    def mutyTredLogn_best_time_true(self):
        self.init_best_time('mutyTredLogn')

    @Rule(Fact(action='FORWARD'),
          Fact(best_time='skip'),
          NOT(Fact(best_time_provided=W())))
    def skipped_best_time_true(self):
        self.init_best_time('', True)

    # Average time
    ####
    def init_average_time(self, name, skipped=False):
        self.declare(Fact(average_time_provided=True))
        self.declare(Fact(average_time_may_depends_on_choice_of_step=(name == 'depends_on_choice_of_step') or skipped))
        self.declare(Fact(average_time_may_be_square=(name == 'square') or skipped))
        self.declare(Fact(average_time_may_be_nlogn=(name == 'nlogn') or skipped))
        self.declare(Fact(average_time_may_be_nlogkn=(name == 'nlogkn') or skipped))
        self.declare(Fact(average_time_may_be_nlgn=(name == 'nlgn') or skipped))
        self.declare(Fact(average_time_may_be_linePlusK=(name == 'linePlusK') or skipped))
        self.declare(Fact(average_time_may_be_nloglogn=(name == 'nloglogn') or skipped))
        self.declare(Fact(average_time_may_be_mutyTredLogn=(name == 'mutyTredLogn') or skipped))

    # Average time lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(average_time='depends_on_choice_of_step'),
          NOT(Fact(average_time_provided=W())))
    def depends_on_choice_of_step_average_time_true(self):
        self.init_average_time('depends_on_choice_of_step')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='square'),
          NOT(Fact(average_time_provided=W())))
    def square_average_time_true(self):
        self.init_average_time('square')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='nlogn'),
          NOT(Fact(average_time_provided=W())))
    def nlogn_average_time_true(self):
        self.init_average_time('nlogn')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='nlogkn'),
          NOT(Fact(average_time_provided=W())))
    def nlogkn_average_time_true(self):
        self.init_average_time('nlogkn')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='nlgn'),
          NOT(Fact(average_time_provided=W())))
    def nlgn_average_time_true(self):
        self.init_average_time('nlgn')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='linePlusK'),
          NOT(Fact(average_time_provided=W())))
    def linePlusK_average_time_true(self):
        self.init_average_time('linePlusK')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='nloglogn'),
          NOT(Fact(average_time_provided=W())))
    def nloglogn_average_time_true(self):
        self.init_average_time('nloglogn')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='mutyTredLogn'),
          NOT(Fact(average_time_provided=W())))
    def mutyTredLogn_average_time(self):
        self.init_average_time('mutyTredLogn')

    @Rule(Fact(action='FORWARD'),
          Fact(average_time='skip'),
          NOT(Fact(average_time_provided=W())))
    def skipped_average_time(self):
        self.init_average_time('', True)

    # Worst time
    ####
    def init_worst_time(self, name, skipped=False):
        self.declare(Fact(worst_time_provided=True))
        self.declare(Fact(worst_time_may_be_square=(name == 'square') or skipped))
        self.declare(Fact(worst_time_may_be_square_unlikely=(name == 'square_unlikely') or skipped))
        self.declare(Fact(worst_time_may_be_nlogn=(name == 'nlogn') or skipped))
        self.declare(Fact(worst_time_may_be_lineMultK=(name == 'lineMultK') or skipped))
        self.declare(Fact(worst_time_may_be_nlgn=(name == 'nlgn') or skipped))
        self.declare(Fact(worst_time_may_be_Ok=(name == 'Ok') or skipped))
        self.declare(Fact(worst_time_may_be_nloglogn=(name == 'nloglogn') or skipped))
        self.declare(Fact(worst_time_may_be_mutyTredLogn=(name == 'mutyTredLogn') or skipped))

    # Worst time lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='square'),
          NOT(Fact(worst_time_provided=W())))
    def square_worst_time_true(self):
        self.init_worst_time('square')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='square_unlikely'),
          NOT(Fact(worst_time_provided=W())))
    def square_unlikely_worst_time_true(self):
        self.init_worst_time('square_unlikely')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='nlogn'),
          NOT(Fact(worst_time_provided=W())))
    def nlogn_worst_time_true(self):
        self.init_worst_time('nlogn')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='lineMultK'),
          NOT(Fact(worst_time_provided=W())))
    def lineMultK_worst_time_true(self):
        self.init_worst_time('lineMultK')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='nlgn'),
          NOT(Fact(worst_time_provided=W())))
    def nlgn_worst_time_true(self):
        self.init_worst_time('nlgn')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='Ok'),
          NOT(Fact(worst_time_provided=W())))
    def Ok_worst_time_true(self):
        self.init_worst_time('Ok')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='nloglogn'),
          NOT(Fact(worst_time_provided=W())))
    def nloglogn_worst_time_true(self):
        self.init_worst_time('nloglogn')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='mutyTredLogn'),
          NOT(Fact(worst_time_provided=W())))
    def mutyTredLogn_worst_time_true(self):
        self.init_worst_time('mutyTredLogn')

    @Rule(Fact(action='FORWARD'),
          Fact(worst_time='skip'),
          NOT(Fact(worst_time_provided=W())))
    def skipped_worst_time(self):
        self.init_worst_time('', True)

    # Memory
    ####
    def init_memory(self, name, skipped=False):
        self.declare(Fact(memory_provided=True))
        self.declare(Fact(memory_may_be_O1=(name == 'O(1)') or skipped))
        self.declare(Fact(memory_may_be_logn=(name == 'O(logn)') or skipped))
        self.declare(Fact(memory_may_be_On_O1=(name == 'OnO1') or skipped))
        self.declare(Fact(memory_may_be_On=(name == 'On') or skipped))
        self.declare(Fact(memory_may_be_Ok=(name == 'Ok') or skipped))
        self.declare(Fact(memory_may_be_On2=(name == 'On2') or skipped))

    # Memory lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(memory='O(1)'),
          NOT(Fact(memory_provided=W())))
    def O1_memory_true(self):
        self.init_memory('O(1)')

    @Rule(Fact(action='FORWARD'),
          Fact(memory='O(logn)'),
          NOT(Fact(memory_provided=W())))
    def logn_memory_true(self):
        self.init_memory('O(logn)')

    @Rule(Fact(action='FORWARD'),
          Fact(memory='OnO1'),
          NOT(Fact(memory_provided=W())))
    def On_O1_memory_true(self):
        self.init_memory('OnO1')

    @Rule(Fact(action='FORWARD'),
          Fact(memory='On'),
          NOT(Fact(memory_provided=W())))
    def On_memory_true(self):
        self.init_memory('On')

    @Rule(Fact(action='FORWARD'),
          Fact(memory='On2'),
          NOT(Fact(memory_provided=W())))
    def On2_memory_true(self):
        self.init_memory('On2')

    @Rule(Fact(action='FORWARD'),
          Fact(memory='skip'),
          NOT(Fact(memory_provided=W())))
    def skipped_memory(self):
        self.init_memory('', True)

    # Sustainability
    ####
    def init_sustainability(self, name, skipped=False):
        self.declare(Fact(sustainability_provided=True))
        self.declare(Fact(sustainability_may_be_yes=(name == 'yes') or skipped))
        self.declare(Fact(sustainability_may_be_no=(name == 'no') or skipped))

    # Sustainability lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(sustainability='yes'),
          NOT(Fact(sustainability_provided=W())))
    def yes_sustainability_true(self):
        self.init_sustainability('yes')

    @Rule(Fact(action='FORWARD'),
          Fact(sustainability='no'),
          NOT(Fact(sustainability_provided=W())))
    def no_sustainability_true(self):
        self.init_sustainability('no')

    @Rule(Fact(action='FORWARD'),
          Fact(sustainability='skip'),
          NOT(Fact(sustainability_provided=W())))
    def skipped_sustainability(self):
        self.init_sustainability('', True)

    # Exchanges
    ####
    def init_exchanges(self, name, skipped=False):
        self.declare(Fact(exchanges_provided=True))
        self.declare(Fact(exchanges_may_be_line=(name == 'line') or skipped))
        self.declare(Fact(exchanges_may_be_square=(name == 'square') or skipped))
        self.declare(Fact(exchanges_may_be_nlogn=(name == 'nlogn') or skipped))
        self.declare(Fact(exchanges_may_be_unresolved=(name == 'unresolved') or skipped))
        self.declare(Fact(exchanges_may_be_linePlusK=(name == 'linePlusK') or skipped))
        self.declare(Fact(exchanges_may_be_nloglogn=(name == 'nloglogn') or skipped))

    # Exchanges lvl1
    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='line'),
          NOT(Fact(exchanges_provided=W())))
    def line_exchanges_true(self):
        self.init_exchanges('line')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='square'),
          NOT(Fact(exchanges_provided=W())))
    def square_exchanges_true(self):
        self.init_exchanges('square')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='nlogn'),
          NOT(Fact(exchanges_provided=W())))
    def nlogn_exchanges_true(self):
        self.init_exchanges('nlogn')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='linePlusK'),
          NOT(Fact(exchanges_provided=W())))
    def linePlusK_exchanges_true(self):
        self.init_exchanges('linePlusK')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='nloglogn'),
          NOT(Fact(exchanges_provided=W())))
    def nloglogn_exchanges_true(self):
        self.init_exchanges('nloglogn')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='unresolved'),
          NOT(Fact(exchanges_provided=W())))
    def unresolved_exchanges_true(self):
        self.init_exchanges('unresolved')

    @Rule(Fact(action='FORWARD'),
          Fact(exchanges='skip'),
          NOT(Fact(exchanges_provided=W())))
    def skipped_exchanges(self):
        self.init_exchanges('', True)

    ### --- ###
    @Rule(Fact(action='FORWARD'),
          Fact(best_time_provided=True),
          Fact(average_time_provided=True),
          Fact(worst_time_provided=True),
          Fact(memory_provided=True),
          Fact(sustainability_provided=True),
          Fact(exchanges_provided=True),
          NOT(Fact(input_finished=W())))
    def input_finished(self):
        self.declare(Fact(input_finished=True))

    ###

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_square=True),
          Fact(worst_time_may_be_square=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_square=True),
          Fact(input_finished=True))
    def bubble_sort_recommended(self):
        self.declare(Fact(bubble_sort_may_be_used=True))
        print("bubble_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_square=True),
          Fact(worst_time_may_be_square=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_square=True),
          Fact(input_finished=True))
    def insertion_sort_recommended(self):
        self.declare(Fact(insertion_sort_may_be_used=True))
        print("insertion_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlog2n=True),
          Fact(average_time_may_depends_on_choice_of_step=True),
          Fact(worst_time_may_be_square=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_square=True),
          Fact(input_finished=True))
    def shellsort_recommended(self):
        self.declare(Fact(shellsort_may_be_used=True))
        print("shellsort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_square=True),
          Fact(average_time_may_be_square=True),
          Fact(worst_time_may_be_square=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_line=True),
          Fact(input_finished=True))
    def selection_sort_recommended(self):
        self.declare(Fact(selection_sort_may_be_used=True))
        print("selection_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlogn=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_square_unlikely=True),
          Fact(memory_may_be_logn=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def quick_sort_recommended(self):
        self.declare(Fact(quick_sort_may_be_used=True))
        print("quick_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlogn=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_On_O1=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def merge_sort_recommended(self):
        self.declare(Fact(merge_sort_may_be_used=True))
        print("merge_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def timsort_recommended(self):
        self.declare(Fact(timsort_may_be_used=True))
        print("timsort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlogn=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def heap_sort_recommended(self):
        self.declare(Fact(heap_sort_be_used=True))
        print("heap_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_O1=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def smoothsort_recommended(self):
        self.declare(Fact(smoothsort_be_used=True))
        print("smoothsort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlogn=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def patience_sorting_recommended(self):
        self.declare(Fact(patience_sorting_be_used=True))
        print("patience_sorting")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_nlogn=True),
          Fact(worst_time_may_be_nlogn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_line=True),
          Fact(input_finished=True))
    def tree_sort_recommended(self):
        self.declare(Fact(tree_sort_be_used=True))
        print("tree_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_linePlusK=True),
          Fact(average_time_may_be_nlogkn=True),
          Fact(worst_time_may_be_lineMultK=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_unresolved=True),
          Fact(input_finished=True))
    def bucket_sort_recommended(self):
        self.declare(Fact(bucket_sort_be_used=True))
        print("bucket_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nlgn=True),
          Fact(average_time_may_be_nlgn=True),
          Fact(worst_time_may_be_nlgn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_unresolved=True),
          Fact(input_finished=True))
    def radix_sort_recommended(self):
        self.declare(Fact(radix_sort_be_used=True))
        print("radix_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_line=True),
          Fact(average_time_may_be_linePlusK=True),
          Fact(worst_time_may_be_Ok=True),
          Fact(memory_may_be_Ok=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_linePlusK=True),
          Fact(input_finished=True))
    def counting_sort_recommended(self):
        self.declare(Fact(counting_sort_be_used=True))
        print("counting_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_nloglogn=True),
          Fact(average_time_may_be_nloglogn=True),
          Fact(worst_time_may_be_nloglogn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_nloglogn=True),
          Fact(input_finished=True))
    def han_sort_recommended(self):
        self.declare(Fact(han_sort_be_used=True))
        print("han_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_mutyTredLogn=True),
          Fact(average_time_may_be_mutyTredLogn=True),
          Fact(worst_time_may_be_mutyTredLogn=True),
          Fact(memory_may_be_On=True),
          Fact(sustainability_may_be_yes=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def multithreaded_merge_sort_recommended(self):
        self.declare(Fact(multithreaded_merge_sort_be_used=True))
        print("multithreaded_merge_sort")

    @Rule(Fact(action='FORWARD'),
          Fact(best_time_may_be_mutyTredLogn=True),
          Fact(average_time_may_be_mutyTredLogn=True),
          Fact(worst_time_may_be_mutyTredLogn=True),
          Fact(memory_may_be_On2=True),
          Fact(sustainability_may_be_no=True),
          Fact(exchanges_may_be_nlogn=True),
          Fact(input_finished=True))
    def PSRS_sorting_recommended(self):
        self.declare(Fact(PSRS_sorting_be_used=True))
        print("PSRS_sorting")

    ### BACKWARD Best time

    ###
    # Line
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(smoothsort_be_used=True), Fact(tree_sort_be_used=True), Fact(counting_sort_be_used=True)),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_line(self):
        self.declare(Fact(backward_best_time_line=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(smoothsort_be_used=True), Fact(tree_sort_be_used=True), Fact(counting_sort_be_used=True)),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_line=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_line_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_line=True))

    ###
    # nlog2n
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(shellsort_may_be_used=True),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_nlog2n(self):
        self.declare(Fact(backward_best_time_nlog2n=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(shellsort_may_be_used=True),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_nlog2n=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlog2n_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_nlog2n=True))

    ###
    # square
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(selection_sort_may_be_used=True),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_square(self):
        self.declare(Fact(backward_best_time_square=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(selection_sort_may_be_used=True),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_square=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_square_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_square=True))

    ###
    # nlogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(patience_sorting_be_used=True)),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_nlogn(self):
        self.declare(Fact(backward_best_time_nlogn=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(patience_sorting_be_used=True)),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_nlogn=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlogn_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_nlogn=True))

    ###
    # linePlusK
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_linePlusK(self):
        self.declare(Fact(backward_best_time_linePlusK=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_linePlusK=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_linePlusK_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_linePlusK=True))

    ###
    # nlgn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_nlgn(self):
        self.declare(Fact(backward_best_time_nlgn=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_nlgn=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlgn_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_nlgn=True))

    ###
    # nloglogn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_nloglogn(self):
        self.declare(Fact(backward_best_time_nloglogn=True))
        self.declare(Fact(backward_best_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_nloglogn=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nloglogn_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_nloglogn=True))

    ###
    # mutyTredLogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(PSRS_sorting_be_used=True), Fact(multithreaded_merge_sort_be_used=True)),
          NOT(Fact(backward_best_time_provided=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_best_time_mutyTredLogn(self):
        self.declare(Fact(backward_best_time_mutyTredLogn=True))
        self.declare(Fact(backward_best_time_provided=True))


    @Rule(Fact(action='BACKWARD'),
          OR(Fact(PSRS_sorting_be_used=True), Fact(multithreaded_merge_sort_be_used=True)),
          Fact(backward_best_time_provided=True),
          NOT(Fact(backward_best_time_mutyTredLogn=W())),
          NOT(Fact(backward_best_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_mutyTredLogn_best_time_skipped(self):
        self.declare(Fact(backward_best_time_skipped=True))
        self.declare(Fact(backward_best_time_mutyTredLogn=True))

    #####
    # BACKWARD best = line
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_line=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_line(self):
        self.declare(Fact(best_time="line"))

    #####
    # BACKWARD best = nlog2n
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_nlog2n=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_nlog2n(self):
        self.declare(Fact(best_time="nlog2n"))

    #####
    # BACKWARD best = square
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_square=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_square(self):
        self.declare(Fact(best_time="square"))

    #####
    # BACKWARD best = nlogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_nlogn=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_nlogn(self):
        self.declare(Fact(best_time="nlogn"))

    #####
    # BACKWARD best = linePlusK
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_linePlusK=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_linePlusK(self):
        self.declare(Fact(best_time="linePlusK"))

    #####
    # BACKWARD best = nlgn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_nlgn=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_nlgn(self):
        self.declare(Fact(best_time="nlgn"))

    #####
    # BACKWARD best = nloglogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_nloglogn=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_nloglogn(self):
        self.declare(Fact(best_time="nloglogn"))

    #####
    # BACKWARD best = mutyTredLogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_provided=True), Fact(backward_best_time_mutyTredLogn=True),
          NOT(Fact(best_time=W())),
          NOT(Fact(backward_best_time_skipped=W())))
    def backward_final_best_time_mutyTredLogn(self):
        self.declare(Fact(best_time="mutyTredLogn"))

    #####
    # BACKWARD best = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_best_time_skipped=True),
          NOT(Fact(best_time=W())))
    def backward_final_best_time_skipped(self):
        self.declare(Fact(best_time="skip"))


    ### BACKWARD Average time

    ###
    # square
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True), Fact(selection_sort_may_be_used=True)),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_square(self):
        self.declare(Fact(backward_average_time_square=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True), Fact(selection_sort_may_be_used=True)),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_square=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_square_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_square=True))

    ###
    # depends_on_choice_of_step
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(shellsort_may_be_used=True),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_depends_on_choice_of_step(self):
        self.declare(Fact(backward_average_time_depends_on_choice_of_step=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(shellsort_may_be_used=True),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_depends_on_choice_of_step=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_depends_on_choice_of_step_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_depends_on_choice_of_step=True))

    ###
    # nlogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(tree_sort_be_used=True)),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_nlogn(self):
        self.declare(Fact(backward_average_time_nlogn=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(tree_sort_be_used=True)),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_nlogn=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlogn_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_nlogn=True))

    ###
    # nlogkn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_nlogkn(self):
        self.declare(Fact(backward_average_time_nlogkn=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_nlogkn=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlogkn_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_nlogkn=True))

    ###
    # nlgn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_nlgn(self):
        self.declare(Fact(backward_average_time_nlgn=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_nlgn=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlgn_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_nlgn=True))

    ###
    # linePlusK
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_linePlusK(self):
        self.declare(Fact(backward_average_time_linePlusK=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_linePlusK=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_linePlusK_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_linePlusK=True))

    ###
    # nloglogn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_nloglogn(self):
        self.declare(Fact(backward_average_time_nloglogn=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_nloglogn=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nloglogn_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_nloglogn=True))

    ###
    # mutyTredLogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          NOT(Fact(backward_average_time_provided=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_average_time_mutyTredLogn(self):
        self.declare(Fact(backward_average_time_mutyTredLogn=True))
        self.declare(Fact(backward_average_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          Fact(backward_average_time_provided=True),
          NOT(Fact(backward_average_time_mutyTredLogn=W())),
          NOT(Fact(backward_average_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_mutyTredLogn_average_time_skipped(self):
        self.declare(Fact(backward_average_time_skipped=True))
        self.declare(Fact(backward_average_time_mutyTredLogn=True))

    #####
    # BACKWARD average = square
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_square=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_square(self):
        self.declare(Fact(average_time="square"))

    #####
    # BACKWARD average = depends_on_choice_of_stepe
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_depends_on_choice_of_step=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_depends_on_choice_of_step(self):
        self.declare(Fact(average_time="depends_on_choice_of_step"))

    #####
    # BACKWARD average = nlogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_nlogn=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_nlogn(self):
        self.declare(Fact(average_time="nlogn"))

    #####
    # BACKWARD average = nlogkn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_nlogkn=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_nlogkn(self):
        self.declare(Fact(average_time="nlogkn"))

    #####
    # BACKWARD average = nlgn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_nlgn=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_nlgn(self):
        self.declare(Fact(average_time="nlgn"))

    #####
    # BACKWARD average = linePlusK
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_linePlusK=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_linePlusK(self):
        self.declare(Fact(average_time="linePlusK"))

    #####
    # BACKWARD average = nloglogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_nloglogn=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_nloglogn(self):
        self.declare(Fact(average_time="nloglogn"))

    #####
    # BACKWARD average = mutyTredLogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_provided=True), Fact(backward_average_time_mutyTredLogn=True),
          NOT(Fact(average_time=W())),
          NOT(Fact(backward_average_time_skipped=W())))
    def backward_final_average_time_mutyTredLogn(self):
        self.declare(Fact(average_time="mutyTredLogn"))

    #####
    # BACKWARD average = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_average_time_skipped=True),
          NOT(Fact(average_time=W())))
    def backward_final_average_time_skipped(self):
        self.declare(Fact(average_time="skip"))

    ### BACKWARD Worst time

    ###
    # square
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True)),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_square(self):
        self.declare(Fact(backward_worst_time_square=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True)),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_square=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_square_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_square=True))

    ###
    # square_unlikely
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(quick_sort_may_be_used=True),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_square_unlikely(self):
        self.declare(Fact(backward_worst_time_square_unlikely=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(quick_sort_may_be_used=True),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_square_unlikely=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_square_unlikely_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_square_unlikely=True))

    ###
    # nlogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True), Fact(tree_sort_be_used=True)),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_nlogn(self):
        self.declare(Fact(backward_worst_time_nlogn=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True), Fact(tree_sort_be_used=True)),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_nlogn=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlogn_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_nlogn=True))

    ###
    # lineMultK
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_lineMultK(self):
        self.declare(Fact(backward_worst_time_lineMultK=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(bucket_sort_be_used=True),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_lineMultK=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_lineMultK_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_lineMultK=True))

    ###
    # nlgn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_nlgn(self):
        self.declare(Fact(backward_worst_time_nlgn=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(radix_sort_be_used=True),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_nlgn=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlgn_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_nlgn=True))

    ###
    # Ok
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_Ok(self):
        self.declare(Fact(backward_worst_time_Ok=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_Ok=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_Ok_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_Ok=True))

    ###
    # nloglogn
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_nloglogn(self):
        self.declare(Fact(backward_worst_time_nloglogn=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_nloglogn=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nloglogn_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_nloglogn=True))

    ###
    # mutyTredLogn
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          NOT(Fact(backward_worst_time_provided=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_worst_time_mutyTredLogn(self):
        self.declare(Fact(backward_worst_time_mutyTredLogn=True))
        self.declare(Fact(backward_worst_time_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          Fact(backward_worst_time_provided=True),
          NOT(Fact(backward_worst_time_mutyTredLogn=W())),
          NOT(Fact(backward_worst_time_skipped=W())),
          NOT(Fact(final=W())))
    def backward_mutyTredLogn_worst_time_skipped(self):
        self.declare(Fact(backward_worst_time_skipped=True))
        self.declare(Fact(backward_worst_time_mutyTredLogn=True))

    #####
    # BACKWARD worst = square
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_square=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_square(self):
        self.declare(Fact(worst_time="square"))

    #####
    # BACKWARD worst = square_unlikely
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_square_unlikely=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_square_unlikely(self):
        self.declare(Fact(worst_time="square_unlikely"))

    #####
    # BACKWARD worst = nlogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_nlogn=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_nlogn(self):
        self.declare(Fact(worst_time="nlogn"))

    #####
    # BACKWARD worst = lineMultK
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_lineMultK=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_lineMultK(self):
        self.declare(Fact(worst_time="lineMultK"))

    #####
    # BACKWARD worst = nlgn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_nlgn=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_nlgn(self):
        self.declare(Fact(worst_time="nlgn"))


    #####
    # BACKWARD worst = Ok
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_Ok=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_Ok(self):
        self.declare(Fact(worst_time="Ok"))

    #####
    # BACKWARD worst = nloglogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_nloglogn=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_nloglogn(self):
        self.declare(Fact(worst_time="nloglogn"))

    #####
    # BACKWARD worst = mutyTredLogn
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_provided=True), Fact(backward_worst_time_mutyTredLogn=True),
          NOT(Fact(worst_time=W())),
          NOT(Fact(backward_worst_time_skipped=W())))
    def backward_final_worst_time_mutyTredLogn(self):
        self.declare(Fact(worst_time="mutyTredLogn"))

    #####
    # BACKWARD worst = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_worst_time_skipped=True),
          NOT(Fact(worst_time=W())))
    def backward_final_worst_time_skipped(self):
        self.declare(Fact(worst_time="skip"))

    ### BACKWARD Memory

    ###
    # O(1)
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(smoothsort_be_used=True)),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_O1(self):
        self.declare(Fact(backward_memory_O1=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True), Fact(heap_sort_be_used=True),
             Fact(smoothsort_be_used=True)),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_O1=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_O1_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_O1=True))

    ###
    # O(logn)
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(quick_sort_may_be_used=True),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_logn(self):
        self.declare(Fact(backward_memory_logn=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(quick_sort_may_be_used=True),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_logn=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_logn_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_logn=True))

    ###
    # OnO1
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(merge_sort_may_be_used=True),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_On_O1(self):
        self.declare(Fact(backward_memory_On_O1=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(merge_sort_may_be_used=True),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_On_O1=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_On_O1_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_On_O1=True))

    ###
    # On
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(timsort_may_be_used=True), Fact(patience_sorting_be_used=True), Fact(tree_sort_be_used=True),
             Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True), Fact(han_sort_be_used=True),
             Fact(multithreaded_merge_sort_be_used=True)),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_On(self):
        self.declare(Fact(backward_memory_On=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(timsort_may_be_used=True), Fact(patience_sorting_be_used=True), Fact(tree_sort_be_used=True),
             Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True), Fact(han_sort_be_used=True),
             Fact(multithreaded_merge_sort_be_used=True)),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_On=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_On_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_On=True))

    ###
    # Ok
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_Ok(self):
        self.declare(Fact(backward_memory_Ok=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_Ok=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_Ok_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_Ok=True))

    ###
    # On2
    ###
    @Rule(Fact(action='BACKWARD'),
          Fact(PSRS_sorting_be_used=True),
          NOT(Fact(backward_memory_provided=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_memory_On2(self):
        self.declare(Fact(backward_memory_On2=True))
        self.declare(Fact(backward_memory_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(PSRS_sorting_be_used=True),
          Fact(backward_memory_provided=True),
          NOT(Fact(backward_memory_On2=W())),
          NOT(Fact(backward_memory_skipped=W())),
          NOT(Fact(final=W())))
    def backward_On2_memory_skipped(self):
        self.declare(Fact(backward_memory_skipped=True))
        self.declare(Fact(backward_memory_On2=True))

    #####
    # BACKWARD memory = O(1)
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_O1=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_O1(self):
        self.declare(Fact(memory="O(1)"))

    #####
    # BACKWARD memory = O(logn)
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_logn=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_logn(self):
        self.declare(Fact(memory="O(logn)"))

    #####
    # BACKWARD memory = OnO1
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_On_O1=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_On_O1(self):
        self.declare(Fact(memory="OnO1"))

    #####
    # BACKWARD memory = On
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_On=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_On(self):
        self.declare(Fact(memory="On"))

    #####
    # BACKWARD memory = Ok
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_Ok=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_Ok(self):
        self.declare(Fact(memory="Ok"))

    #####
    # BACKWARD memory = On2
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_provided=True), Fact(backward_memory_On2=True),
          NOT(Fact(memory=W())),
          NOT(Fact(backward_memory_skipped=W())))
    def backward_final_memory_On2(self):
        self.declare(Fact(memory="On2"))

    #####
    # BACKWARD memory = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_memory_skipped=True),
          NOT(Fact(memory=W())))
    def backward_final_memory_skipped(self):
        self.declare(Fact(memory="skip"))

    ### BACKWARD Sustainability

    ###
    # Sustainability yes
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True), Fact(tree_sort_be_used=True),
             Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True), Fact(counting_sort_be_used=True),
             Fact(han_sort_be_used=True), Fact(multithreaded_merge_sort_be_used=True)),
          NOT(Fact(backward_sustainability_provided=W())),
          NOT(Fact(backward_sustainability_skipped=W())),
          NOT(Fact(final=W())))
    def backward_sustainability_yes(self):
        self.declare(Fact(backward_sustainability_yes=True))
        self.declare(Fact(backward_sustainability_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True), Fact(tree_sort_be_used=True),
             Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True), Fact(counting_sort_be_used=True),
             Fact(han_sort_be_used=True), Fact(multithreaded_merge_sort_be_used=True)),
          Fact(backward_sustainability_provided=True),
          NOT(Fact(backward_sustainability_yes=W())),
          NOT(Fact(backward_sustainability_skipped=W())),
          NOT(Fact(final=W())))
    def backward_yes_sustainability_skipped(self):
        self.declare(Fact(backward_sustainability_skipped=True))
        self.declare(Fact(backward_sustainability_yes=True))

    ###
    # Sustainability no
    ###
    @Rule(Fact(action='BACKWARD'),
          OR(Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True), Fact(quick_sort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(PSRS_sorting_be_used=True)),
          NOT(Fact(backward_sustainability_provided=W())),
          NOT(Fact(backward_sustainability_skipped=W())),
          NOT(Fact(final=W())))
    def backward_sustainability_no(self):
        self.declare(Fact(backward_sustainability_no=True))
        self.declare(Fact(backward_sustainability_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(shellsort_may_be_used=True), Fact(selection_sort_may_be_used=True), Fact(quick_sort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(PSRS_sorting_be_used=True)),
          Fact(backward_sustainability_provided=True),
          NOT(Fact(backward_sustainability_no=W())),
          NOT(Fact(backward_sustainability_skipped=W())),
          NOT(Fact(final=W())))
    def backward_no_sustainability_skipped(self):
        self.declare(Fact(backward_sustainability_skipped=True))
        self.declare(Fact(backward_sustainability_no=True))

    #####
    # BACKWARD sustainability = yes
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_sustainability_provided=True), Fact(backward_sustainability_yes=True),
          NOT(Fact(sustainability=W())),
          NOT(Fact(backward_sustainability_skipped=W())))
    def backward_final_sustainability_yes(self):
        self.declare(Fact(sustainability="Yes"))

    #####
    # BACKWARD sustainability = no
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_sustainability_provided=True), Fact(backward_sustainability_no=True),
          NOT(Fact(sustainability=W())),
          NOT(Fact(backward_sustainability_skipped=W())))
    def backward_final_sustainability_no(self):
        self.declare(Fact(sustainability="No"))

    #####
    # BACKWARD sustainability = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_sustainability_skipped=True),
          NOT(Fact(sustainability=W())))
    def backward_final_sustainability_skipped(self):
        self.declare(Fact(sustainability="skip"))


    ### BACKWARD Exchanges

    ###
    # line
    ###

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(selection_sort_may_be_used=True), Fact(tree_sort_be_used=True)),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_line(self):
        self.declare(Fact(backward_exchanges_line=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(selection_sort_may_be_used=True), Fact(tree_sort_be_used=True)),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_line=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_line_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_line=True))

    ###
    # square
    ###

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True)),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_square(self):
        self.declare(Fact(backward_exchanges_square=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bubble_sort_may_be_used=True), Fact(insertion_sort_may_be_used=True),
             Fact(shellsort_may_be_used=True)),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_square=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_square_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_square=True))

    ###
    # nlogn
    ###

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_nlogn(self):
        self.declare(Fact(backward_exchanges_nlogn=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(quick_sort_may_be_used=True), Fact(merge_sort_may_be_used=True), Fact(timsort_may_be_used=True),
             Fact(heap_sort_be_used=True), Fact(smoothsort_be_used=True), Fact(patience_sorting_be_used=True),
             Fact(multithreaded_merge_sort_be_used=True), Fact(PSRS_sorting_be_used=True)),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_nlogn=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nlogn_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_nlogn=True))

    ###
    # unresolved
    ###

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True)),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_unresolved(self):
        self.declare(Fact(backward_exchanges_unresolved=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          OR(Fact(bucket_sort_be_used=True), Fact(radix_sort_be_used=True)),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_unresolved=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_unresolved_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_unresolved=True))

    ###
    # linePlusK
    ###

    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_linePlusK(self):
        self.declare(Fact(backward_exchanges_linePlusK=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(counting_sort_be_used=True),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_linePlusK=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_linePlusK_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_linePlusK=True))

    ###
    # nloglogn
    ###

    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          NOT(Fact(backward_exchanges_provided=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_exchanges_nloglogn(self):
        self.declare(Fact(backward_exchanges_nloglogn=True))
        self.declare(Fact(backward_exchanges_provided=True))

    @Rule(Fact(action='BACKWARD'),
          Fact(han_sort_be_used=True),
          Fact(backward_exchanges_provided=True),
          NOT(Fact(backward_exchanges_nloglogn=W())),
          NOT(Fact(backward_exchanges_skipped=W())),
          NOT(Fact(final=W())))
    def backward_nloglogn_exchanges_skipped(self):
        self.declare(Fact(backward_exchanges_skipped=True))
        self.declare(Fact(backward_exchanges_nloglogn=True))

    #####
    # BACKWARD exchanges = line
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_line=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_line(self):
        self.declare(Fact(exchanges="line"))

    #####
    # BACKWARD exchanges = square
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_square=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_square(self):
        self.declare(Fact(exchanges="square"))

    #####
    # BACKWARD exchanges = nlogn
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_nlogn=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_nlogn(self):
        self.declare(Fact(exchanges="nlogn"))


    #####
    # BACKWARD exchanges = unresolved
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_unresolved=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_unresolved(self):
        self.declare(Fact(exchanges="unresolved"))

    #####
    # BACKWARD exchanges = linePlusK
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_linePlusK=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_linePlusK(self):
        self.declare(Fact(exchanges="linePlusK"))

    #####
    # BACKWARD exchanges = nloglogn
    #####

    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_provided=True), Fact(backward_exchanges_nloglogn=True),
          NOT(Fact(exchanges=W())),
          NOT(Fact(backward_exchanges_skipped=W())))
    def backward_final_exchanges_nloglogn(self):
        self.declare(Fact(exchanges="nloglogn"))


    #####
    # BACKWARD exchanges = skipped
    #####
    @Rule(Fact(action='BACKWARD'), Fact(final=True),
          Fact(backward_exchanges_skipped=True),
          NOT(Fact(exchanges=W())))
    def backward_final_exchanges_skipped(self):
        self.declare(Fact(exchanges="skip"))





